<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;
    /**
     * fillable
     *
     * @var array
     */
    // protected $primaryKey = 'location_id';
    protected $fillable = [
        'location_name',
    ];

    public function employees()
    {
        return $this->hasMany(Employee::class, 'location_id');
    }
}
