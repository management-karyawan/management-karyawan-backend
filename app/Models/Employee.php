<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Employee extends Model
{
    use HasFactory;

    /**
     * fillable
     *
     * @var array
     */
    protected $fillable = [
        'employee_position_id',
        'location_id',
        'image',
        'name',
        'gender',
        'birthdate',
        'email',
        'cellphone',
        'status_pegawai',
    ];

    /**
     * image
     *
     * @return Attribute
     */
    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn ($image) => url('/storage/employees/' . $image),
        );
    }

     public function employeePosition()
     {
         return $this->belongsTo(EmployeePosition::class, 'employee_position_id');
     }
 
     public function location()
     {
         return $this->belongsTo(Location::class, 'location_id');
     }
    
}
