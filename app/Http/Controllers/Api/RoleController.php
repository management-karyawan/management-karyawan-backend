<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MessageResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
     /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $users = User::latest()->paginate(5);

        return new MessageResource(true, 'List Data Role', $users);
    }

     /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:8|confirmed',
            'telephone' => 'required|min:11'
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }


        //create post
        $user= User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
            'telephone'  => $request->telephone
        ]);

        //return response
        return new MessageResource(true, 'Data Role Berhasil Ditambahkan!', $user);
    }

    public function update(Request $request, $id)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'role'      => 'required',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //find post by ID
        $user = User::find($id);

        $user->update([
            'role' => $request->role,
        ]);
         

       
        return new MessageResource(true, 'Data Role Berhasil Diubah!', $user);
    }

    public function destroy($id)
    {

        //find post by ID
        $user = User::find($id);

        //delete post
        $user->delete();

        //return response
        return new MessageResource(true, 'Data Role Berhasil Dihapus!', null);
    }
}
