<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MessageResource;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;



class LocationController extends Controller
{

     /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $locations = Location::latest()->paginate(5);

        return new MessageResource(true, 'List Data Location', $locations);
    }

     /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'location_name'     => 'required',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }


        //create post
        $location= Location::create([
            'location_name'   => $request->location_name,
        ]);

        //return response
        return new MessageResource(true, 'Data Location Berhasil Ditambahkan!', $location);
    }

    public function update(Request $request, $id)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'location_name'   => 'required',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //find post by ID
        $location = Location::find($id);

        $location->update([
            'location_name' => $request->location_name,
        ]);
         

       
        return new MessageResource(true, 'Data Location Berhasil Diubah!', $location);
    }

    public function destroy($id)
    {

        //find post by ID
        $location = Location::find($id);

        //delete post
        $location->delete();

        //return response
        return new MessageResource(true, 'Data Location Berhasil Dihapus!', null);
    }
}
