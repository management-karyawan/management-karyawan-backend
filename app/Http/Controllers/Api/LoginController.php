<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;



class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'     => 'required_without:telephone|email',
            'telephone' => 'required_without:email|numeric',
            'password'  => 'required'
        ]);
    
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
    
        $credentials = $request->only('password');
        if ($request->has('email')) {
            $credentials['email'] = $request->email;
        } else {
            $credentials['telephone'] = $request->telephone;
        }
    
        $user = User::where('email', $request->email)
                    ->orWhere('telephone', $request->telephone)
                    ->first();
    
        if ($user && Hash::check($request->password, $user->password)) {
            if (!$token = auth()->guard('api')->login($user)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Autentikasi gagal'
                ], 401);
            }
    
            return response()->json([
                'success' => true,
                'user'    => $user,
                'token'   => $token
            ], 200);
        }
    
        return response()->json([
            'success' => false,
            'message' => 'Email atau Telepon dan Password Anda salah'
        ], 401);
                

    }
}
