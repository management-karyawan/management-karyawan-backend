<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MessageResource;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $employees = Employee::latest()->paginate(5);

        return new MessageResource(true, 'List Data Employee', $employees);
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'image'     => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name'     => 'required', 
            'gender'   => 'required',
            'birthdate' => 'required',
            'email' => 'required',
            'cellphone' => 'required',
            'status_pegawai' => 'required',
            'employee_position_id' => 'required|exists:employee_positions,id',
            'location_id' => 'required|exists:locations,id',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //upload image
        $image = $request->file('image');
        $image->storeAs('public/avatar', $image->hashName());

        //create employee
        $employee = Employee::create([
            'image' => $image->hashName(),
            'name' => $request->name,
            'gender' => $request->gender,
            'birthdate' => $request->birthdate,
            'email' => $request->email,
            'cellphone' => $request->cellphone,
            'status_pegawai' => $request->status_pegawai,
            'employee_position_id' => $request->employee_position_id,
            'location_id' => $request->location_id,
        ]);

        //return response
        return new MessageResource(true, 'Data Employee Berhasil Ditambahkan!', $employee);
    }

    public function show($id)
    {
      
        $employee = Employee::find($id);

    
        return new MessageResource(true, 'Detail Data Employee!', $employee);
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'name'     => 'required', 
            'gender'   => 'required',
            'birthdate' => 'required',
            'email' => 'required',
            'cellphone' => 'required',
            'status_pegawai' => 'required',
            'employee_position_id' => 'required|exists:employee_positions,id',
            'location_id' => 'required|exists:locations,id',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //find employee by ID
        $employee = Employee::find($id);

        //check if image is not empty
        if ($request->hasFile('image')) {

            //upload image
            $image = $request->file('image');
            $image->storeAs('public/avatar', $image->hashName());

            //delete old image
            Storage::delete('public/avatar/' . basename($employee->image));

            //update employee with new image
            $employee->update([
                'image'     => $image->hashName(),
                'name' => $request->name,
                'gender' => $request->gender,
                'birthdate' => $request->birthdate,
                'email' => $request->email,
                'cellphone' => $request->cellphone,
                'status_pegawai' => $request->status_pegawai,
                'employee_position_id' => $request->employee_position_id,
                'location_id' => $request->location_id,
            ]);
        } else {

            //update employee without image
            $employee->update([
                'name' => $request->name,
                'gender' => $request->gender,
                'birthdate' => $request->birthdate,
                'email' => $request->email,
                'cellphone' => $request->cellphone,
                'status_pegawai' => $request->status_pegawai,
                'employee_position_id' => $request->employee_position_id,
                'location_id' => $request->location_id,
            ]);
        }

        //return response
        return new MessageResource(true, 'Data Employee Berhasil Diubah!', $employee);
    }

    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {

        $employee = Employee::find($id);

        Storage::delete('public/avatar/'.basename($employee->image));

        $employee->delete();

        return new MessageResource(true, 'Data Post Berhasil Dihapus!', null);
    }
}
