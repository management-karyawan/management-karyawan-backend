<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EmployeePositionResource;
use App\Models\EmployeePosition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class EmployeePositionController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $positions = EmployeePosition::latest()->paginate(5);

        return new EmployeePositionResource(true, 'List Data Employee Position', $positions);
    }

     /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'position_name'     => 'required',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }


        //create post
        $position= EmployeePosition::create([
            'position_name'   => $request->position_name,
        ]);

        //return response
        return new EmployeePositionResource(true, 'Data Location Berhasil Ditambahkan!', $position);
    }

    public function update(Request $request, $id)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'position_name'   => 'required',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //find post by ID
        $position = EmployeePosition::find($id);

        $position->update([
            'position_name' => $request->position_name,
        ]);
         

       
        return new EmployeePositionResource(true, 'Data Location Berhasil Diubah!', $position);
    }

    public function destroy($id)
    {

        //find post by ID
        $position = EmployeePosition::find($id);

        //delete post
        $position->delete();

        //return response
        return new EmployeePositionResource(true, 'Data Location Berhasil Dihapus!', null);
    }
}
