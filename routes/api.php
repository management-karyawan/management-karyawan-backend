<?php

use App\Http\Controllers\Api\ChangePasswordController;
use App\Http\Controllers\Api\PasswordResetRequestController;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware(Authenticate::using('sanctum'));

// Route::group(['middleware' => 'api','prefix' => 'auth'], function ($router) {

//     Route::post('register', [AuthController::class, 'register'] );
//     Route::post('login', [AuthController::class, 'login'] );
//     Route::post('logout', [AuthController::class, 'logout']  );
//     Route::post('refresh', [AuthController::class, 'refresh']  );
//     Route::post('me', [AuthController::class, 'me']  );

// });

Route::post('/register', App\Http\Controllers\Api\RegisterController::class)->name('register');
Route::post('/login', App\Http\Controllers\Api\LoginController::class)->name('login');
/**
 * route "/user"
 * @method "GET"
 */
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/sendPasswordResetLink', [PasswordResetRequestController::class, 'sendEmail'])->name('sendEmail');
Route::post('/resetPassword', [ChangePasswordController::class, 'passwordResetProcess'])->name('passwordResetProcess');
Route::post('/logout', App\Http\Controllers\Api\LogoutController::class)->name('logout');
Route::apiResource('/employees', App\Http\Controllers\Api\EmployeeController::class);
Route::apiResource('/employee-positions', App\Http\Controllers\Api\EmployeePositionController::class);
Route::apiResource('/locations', App\Http\Controllers\Api\LocationController::class);
Route::apiResource('/roles', App\Http\Controllers\Api\RoleController::class);
