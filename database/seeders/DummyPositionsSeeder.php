<?php

namespace Database\Seeders;

use App\Models\EmployeePosition;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DummyPositionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $positionData = [ 
            [
                'position_name'=>'Analyst'
            ],
            [
                'position_name'=>'Manajer'
            ],
            [
                'position_name'=>'Sales'
            ],
            [
                'position_name'=>'Staff'
            ],
            [
                'position_name'=>'Direktur'
            ],
            [
                'position_name'=>'Pergudangan'
            ],
            [
                'position_name'=>'HRD'
            ],
        ];

        foreach($positionData as $key => $val){
            EmployeePosition::create($val);
        }
    }
}
