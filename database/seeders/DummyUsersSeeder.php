<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DummyUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $userData = [ 
            [
                'name'=>'SuperAdmin',
                'email'=>'superadmin@gmail.com',
                'role'=>'superadmin',
                'telephone'=>'08123456789',
                'password'=>bcrypt('superadmin123')
            ],
            [
                'name'=>'Admin',
                'email'=>'admin@gmail.com',
                'role'=>'admin',
                'telephone'=>'08123456788',
                'password'=>bcrypt('admin123')
            ],
            [
                'name'=>'Karyawan',
                'email'=>'karyawan1@gmail.com',
                'role'=>'karyawan',
                'telephone'=>'08123456787',
                'password'=>bcrypt('karyawan123')
            ],
        ];

        foreach($userData as $key => $val){
            User::create($val);
        }
    }
}
