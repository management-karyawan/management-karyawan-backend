<?php

namespace Database\Seeders;

use App\Models\Location;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DummyLocationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $locationData = [ 
            [
                'location_name'=>'Jakarta'
            ],
            [
                'location_name'=>'Bandung'
            ],
            [
                'location_name'=>'Surabaya'
            ],
            [
                'location_name'=>'Yogyakarta'
            ],
            [
                'location_name'=>'Semarang'
            ],
            [
                'location_name'=>'Bekasi'
            ],
            [
                'location_name'=>'Tangerang'
            ],
            [
                'location_name'=>'Banten'
            ],
            [
                'location_name'=>'Makasar'
            ],
            [
                'location_name'=>'Balikpapan'
            ],
            [
                'location_name'=>'Malang'
            ],
            [
                'location_name'=>'Banjarmasin'
            ],
            [
                'location_name'=>'Bali'
            ],
            [
                'location_name'=>'Madura'
            ],
            [
                'location_name'=>'Manado'
            ],
        ];

        foreach($locationData as $key => $val){
            Location::create($val);
        }
    }
}
