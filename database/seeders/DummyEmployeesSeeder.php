<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DummyEmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $employeeData = [ 
            [
                'image' => 'avatar.png',
                'name'=>'Helena',
                'gender'=>'Perempuan',
                'birthdate'=>'01-02-1999',
                'email'=>'helen@gmail.com',
                'cellphone'=>'08123456789',
                'status_pegawai'=>'kontrak',
                'employee_position_id'=>'2',
                'location_id'=>'1' 
            ],
            [
                'image' => 'avatar.png',
                'name'=>'Karmila',
                'gender'=>'Perempuan',
                'birthdate'=>'01-02-1989',
                'email'=>'mila@gmail.com',
                'cellphone'=>'08123456779',
                'status_pegawai'=>'kontrak',
                'employee_position_id'=>'2',
                'location_id'=>'3' 
            ],
            [
                'image' => 'avatar.png',
                'name'=>'Desi',
                'gender'=>'Perempuan',
                'birthdate'=>'01-02-1998',
                'email'=>'des@gmail.com',
                'cellphone'=>'08123416789',
                'status_pegawai'=>'kontrak',
                'employee_position_id'=>'2',
                'location_id'=>'4' 
            ],
            [
                'image' => 'avatar.png',
                'name'=>'Albert',
                'gender'=>'Laki-Laki',
                'birthdate'=>'01-06-1998',
                'email'=>'albert@gmail.com',
                'cellphone'=>'08123416189',
                'status_pegawai'=>'kontrak',
                'employee_position_id'=>'1',
                'location_id'=>'4' 
            ],
            [
                'image' => 'avatar.png',
                'name'=>'Budi',
                'gender'=>'Laki-Laki',
                'birthdate'=>'01-08-1998',
                'email'=>'budi@gmail.com',
                'cellphone'=>'08123416766',
                'status_pegawai'=>'kontrak',
                'employee_position_id'=>'2',
                'location_id'=>'1' 
            ],
        ];

        foreach($employeeData as $key => $val){
            Employee::create($val);
        }
    }
}
